package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"sync"
	"time"
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"github.com/hashicorp/go-retryablehttp"
)

// Possible stocks to check. I'd use a database (or maybe file) if I needed to index a lot of values.
var stocks = map[string]string{
	"AONIP": "AONIP",
	"FB"   : "FB",
	"APLE" : "APLE",
	"INTC" : "INTC",
	"TSLA" : "TSLA",
	"MSFT" : "MSFT",
	"TWTR" : "TWTR",
	"FNKO" : "FNKO",
	"WORK" : "WORK",
	"AMD"  : "AMD",
}

type StockRequest struct {
	Stocks []string `json:"stocks"`
}

type StockEntry struct {
	Symbol  string         `json:"symbol"`
	Name    string         `json:"name"`
	History []StockHistory `json:"history"`
}

type StockHistory struct {
	Date          time.Time `json:"date"`
	OpenPrice     float32   `json:"openPrice"`
	ClosePrice    float32   `json:"closePrice"`
	TradingVolume int       `json:"volume"`
}

const dateFormat = "2006-01-02"

func (s *StockHistory) UnmarshalJSON(p []byte) error {
	var aux struct {
		Date          string  `json:"date"`
		OpenPrice     float32 `json:"openPrice"`
		ClosePrice    float32 `json:"closePrice`
		TradingVolume int     `json:"volume"`
	}

	err := json.Unmarshal(p, &aux)
	if err != nil {
		return err
	}

	t, err := time.Parse(dateFormat, aux.Date)
	if err != nil {
		return err
	}

	s.Date          = t
	s.OpenPrice     = aux.OpenPrice
	s.ClosePrice    = aux.ClosePrice
	s.TradingVolume = aux.TradingVolume

	return nil
}

// Since we are making more than one external request per request to our API,
// collect errors and return whatever data we can successfully get.
type StockResponse struct {
	StockData map[string]FilteredStockEntry `json:"stockData"`
	Errors    []string                      `json:"errors"`
}

func GetStock(symbol string) (StockEntry, error) {
	var entry StockEntry

	url := fmt.Sprintf("https://stock-tracker.dev.ip.aon.com/stocks/%s", symbol)
	resp, err := retryablehttp.Get(url)
	if err != nil {
		fmt.Println("GetStock resp error: ", err)
		return entry, err
	}

	error := json.NewDecoder(resp.Body).Decode(&entry)
	if error != nil {
		fmt.Println("GetStock decode error: ", error)
		return entry, error
	}

	return entry, nil
}

type FilteredStockEntry struct {
	Name                 string       `json:"name"`
	Symbol               string       `json:"symbol"`
	MostRecentDay        StockHistory `json:"mostRecentDay"`
	YearHighOpen         StockHistory `json:"yearHighOpen"`
	YearHighClose        StockHistory `json:"yearHighClose"`
	YearLowOpen          StockHistory `json:"yearLowOpen"`
	YearLowClose         StockHistory `json:"yearLowClose"`
	AverageTradingVolume float32      `json:"averageTradingVolume"`
}

func low(s []StockHistory, prop string) (int, error) {
	var index int

	if prop == "open" {
		for i := len(s)-1; i >= 0; i-- {
			if s[i].OpenPrice >= 0 {
				index = i
				return index, nil
			}
		}
	} else if prop == "close" {
		for i := len(s)-1; i >= 0; i-- {
			if s[i].ClosePrice >= 0 {
				index = i
				return index, nil
			}
		}
	} else {
		return index, errors.New("func low() error: incompatible prop (use 'open' or 'close'")
	}


	return index, errors.New("func low() error: lowest price is negative")
}

// Currently not dealing with bad data like negative prices.
// Handle / disregard entries with bad data while sorting / filtering.
func FilterStock(stockEntry StockEntry) (FilteredStockEntry, error) {
	var filteredStock FilteredStockEntry

	filteredStock.Name   = stockEntry.Name
	filteredStock.Symbol = stockEntry.Symbol

	sort.Slice(stockEntry.History, func(i, j int) bool { 
		return stockEntry.History[i].Date.After(stockEntry.History[j].Date)
	})

	// Get the average of all trading volumes for the stock
	var volumeTotal float32
	for _, entry := range stockEntry.History {
		volumeTotal += float32(entry.TradingVolume)
	}
	average := volumeTotal / float32(len(stockEntry.History))
	filteredStock.AverageTradingVolume = average

	// Find the most recent entry with a positive close price
	i := 0
	for stockEntry.History[i].ClosePrice < 0 {
		i++
	}
	filteredStock.MostRecentDay = stockEntry.History[i]

	// keep 365 days worth of entries
	if len(stockEntry.History) > 365 {
		var year []StockHistory
		year = stockEntry.History[0:365]
		stockEntry.History = year
	}

	// Sort by OpenPrice
	sort.Slice(stockEntry.History, func(i, j int) bool {
		return stockEntry.History[i].OpenPrice > stockEntry.History[j].OpenPrice
	})

	// Find the entry with the high open for the year (52 weeks)
	filteredStock.YearHighOpen = stockEntry.History[0]

	// Find the lowest non-negative opening price
	lowOpenIndex, err := low(stockEntry.History, "open")
	if err != nil {
		fmt.Println(err)
		return filteredStock, err
	}

	filteredStock.YearLowOpen = stockEntry.History[lowOpenIndex]


	// Sort by ClosePrice
	sort.Slice(stockEntry.History, func(i, j int) bool {
		return stockEntry.History[i].ClosePrice > stockEntry.History[j].ClosePrice
	})

	// Find the entry with the high close for the year (52 weeks)
	filteredStock.YearHighClose = stockEntry.History[0]

	// Find the lowest non-negative closing price
	lowCloseIndex, err := low(stockEntry.History, "close")
	if err != nil {
		fmt.Println(err)
		return filteredStock, err
	}

	filteredStock.YearLowClose = stockEntry.History[lowCloseIndex]


	return filteredStock, nil
}

func StockHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers",
        "Accept, Content-Type, Content-Length, Accept-Encoding")
    w.Header().Set("Content-Type", "application/json")

	var stockRequest StockRequest
	var errs []string

	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &stockRequest)
	
	var rawStockData []StockEntry
	var wg sync.WaitGroup
	var mutex = &sync.Mutex{}

	for _, symbol := range stockRequest.Stocks {
		if val, ok := stocks[symbol]; ok {
			wg.Add(1)
			go func(sym string){
				defer wg.Done()
				s, err := GetStock(sym)
				if err != nil {
					fmt.Println("GetStock err: ", err)
					mutex.Lock()
					errs = append(errs, err.Error())
					mutex.Unlock()
					return
				}

				mutex.Lock()
				rawStockData = append(rawStockData, s)
				mutex.Unlock()
			}(val)
		}
	}
	wg.Wait()

	var filteredStockData = map[string]FilteredStockEntry{}
	for _, rawStock := range rawStockData {
		f, _ := FilterStock(rawStock)
		filteredStockData[rawStock.Symbol] = f
	}

	var response StockResponse
	response.StockData = filteredStockData
	response.Errors    = errs

	resp, _ := json.Marshal(response)
	w.Write(resp)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/stocks", StockHandler).Methods("POST")

	fmt.Println("Listening at port 8000")

	http.ListenAndServe(":8000", handlers.CORS(
		handlers.AllowedMethods([]string{"POST", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedHeaders([]string{
			"Content-Type", "X-Requested-With", "Access-Control-Allow-Origin", "Origin",
			"Accept", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization",
		}),
	)(router))
}
