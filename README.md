# Setup
To run this project, a working Go installation is required: 
- [Download Go](https://golang.org/)
- Clone the repository to your ~/go/src/ directory
- Run `go mod download` to download dependencies
- Run `go build` to compile
- Run `./ticker-api` to start the API at localhost:8000

# Usage
The API has a single endpoint that accepts a POST request at `/stocks`,
Content-Type: application/json

Body example:
```
{
    "stocks": ["FB", "MSFT"]
}
```

Only stock symbols from this list will return data:
```
    "AONIP",
    "FB",
    "APLE",
    "TSLA",
    "MSFT",
    "TWTR",
    "FNKO",
    "WORK",
    "AMD"
```

A complete request from the command line will look something like this (I'm using [HTTpie](https://httpie.org/)):
```
> HTTP POST localhost:8000/stocks @stocks.json
```

Unsupported stock tickers will be ignored, with an error added to an 'errors' property in the response.
This will allow valid stocks to be processed even if invalid ticker symbols are mixed in.
An example response will look like this: 
```
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Accept, Content-Type, Content-Length, Accept-Encoding
Access-Control-Allow-Methods: POST
Content-Length: 607
Content-Type: application/json
Date: Mon, 21 Sep 2020 00:00:40 GMT

{
    "errors": null,
    "stockData": {
        "FB": {
            "averageTradingVolume": 4974039.5,
            "mostRecentDay": {
                "closePrice": 253.28,
                "date": "2020-09-20T00:00:00Z",
                "openPrice": 252.35,
                "volume": 8653515
            },
            "name": "Facebook",
            "symbol": "FB",
            "yearHighClose": {
                "closePrice": 495.55,
                "date": "2020-08-09T00:00:00Z",
                "openPrice": 493.46,
                "volume": 8594544
            },
            "yearHighOpen": {
                "closePrice": 491.56,
                "date": "2020-04-25T00:00:00Z",
                "openPrice": 494.52,
                "volume": 438360
            },
            "yearLowClose": {
                "closePrice": 1.6,
                "date": "2020-04-11T00:00:00Z",
                "openPrice": 11.5,
                "volume": 7878675
            },
            "yearLowOpen": {
                "closePrice": 4.67,
                "date": "2020-01-11T00:00:00Z",
                "openPrice": 1.38,
                "volume": 3703838
            }
        }
    }
}

```

# Notes
The API uses gorilla/mux for routing, becuase I like the syntax for routing / headers /etc the library provides.

hashicorp/go-retryablehttp adds a modified net/http client that includes retries / exponential backoffs for failed requests.

All other libraries are standard Go libraries.

